package main

import (
	"fmt"
	"os/exec"
)

// var shellPathOut string = fmt.Sprintf("%s%s", mainconfig.ScriptsLocation, "honeoutsidedoor.sh")
// var shellPathIn string = fmt.Sprintf("%s%s", mainconfig.ScriptsLocation, "inside_door_hone.sh")

// TODO implement system that uses the script list in the config file

func openOutside(ScriptsLocation string) error {
	var shellPathOut string = fmt.Sprintf("%s%s", ScriptsLocation, "honeoutsidedoor.sh")

	cmd := exec.Command(shellPathOut)
	return cmd.Run()
}

func openInside(ScriptsLocation string) error {
	var shellPathIn string = fmt.Sprintf("%s%s", ScriptsLocation, "inside_door_hone.sh")

	cmd := exec.Command(shellPathIn)
	return cmd.Run()
}
